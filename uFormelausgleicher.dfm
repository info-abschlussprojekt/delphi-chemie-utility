object Form1: TForm1
  Left = 0
  Top = 0
  ActiveControl = PageControl1
  Caption = 'Chemistry Utility'
  ClientHeight = 551
  ClientWidth = 960
  Color = clWhite
  Constraints.MaxHeight = 590
  Constraints.MaxWidth = 976
  Constraints.MinHeight = 590
  Constraints.MinWidth = 976
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LHelp: TLabel
    Left = 931
    Top = 530
    Width = 21
    Height = 13
    Caption = 'Hilfe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = LHelpClick
  end
  object LWebsite: TLabel
    Left = 835
    Top = 530
    Width = 78
    Height = 13
    Caption = 'Website-Version'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = LWebsiteClick
  end
  object Label7: TLabel
    Left = 671
    Top = 530
    Width = 158
    Height = 13
    Caption = 'Nutzung der Website empfohlen:'
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 4
    Width = 977
    Height = 520
    ActivePage = FormulaBalancer
    TabHeight = 30
    TabOrder = 0
    TabWidth = 170
    object FormulaBalancer: TTabSheet
      Caption = 'Reaktionsausgleicher'
      ImageIndex = 1
      object LOutput: TLabel
        Left = 3
        Top = 96
        Width = 3
        Height = 13
      end
      object LOutputHead: TLabel
        Left = 3
        Top = 69
        Width = 118
        Height = 13
        Caption = 'Ausgeglichene Reaktion:'
        Visible = False
      end
      object EInput: TEdit
        Left = 3
        Top = 34
        Width = 684
        Height = 21
        TabOrder = 0
        TextHint = 'Auszugleichende Reaktion'
        OnKeyUp = EInputKeyUp
      end
      object BStart: TButton
        Left = 693
        Top = 31
        Width = 121
        Height = 28
        Caption = 'Start'
        TabOrder = 1
        OnClick = BStartClick
      end
    end
    object PeriodicTable: TTabSheet
      Caption = 'Liste der Elemente'
      ImageIndex = 1
      object StringGrid1: TStringGrid
        Left = -4
        Top = 3
        Width = 965
        Height = 478
        ColCount = 10
        RowCount = 119
        TabOrder = 0
        ColWidths = (
          85
          64
          96
          119
          126
          80
          98
          84
          97
          82)
      end
    end
    object Stoichiometry: TTabSheet
      Caption = 'St'#246'chiometrie'
      ImageIndex = 2
      object Label1: TLabel
        Left = 0
        Top = 22
        Width = 33
        Height = 13
        Caption = 'Stoff 1'
      end
      object Label2: TLabel
        Left = 200
        Top = 22
        Width = 39
        Height = 13
        Caption = 'Masse 1'
      end
      object Label3: TLabel
        Left = 408
        Top = 22
        Width = 65
        Height = 13
        Caption = 'Stoffmenge 1'
      end
      object Label4: TLabel
        Left = 0
        Top = 64
        Width = 33
        Height = 13
        Caption = 'Stoff 2'
      end
      object Label5: TLabel
        Left = 200
        Top = 64
        Width = 39
        Height = 13
        Caption = 'Masse 2'
      end
      object Label6: TLabel
        Left = 408
        Top = 64
        Width = 65
        Height = 13
        Caption = 'Stoffmenge 2'
      end
      object ESubstance1: TEdit
        Left = 39
        Top = 19
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object EMass1: TEdit
        Left = 245
        Top = 19
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object EAmount1: TEdit
        Left = 479
        Top = 19
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object ESubstance2: TEdit
        Left = 39
        Top = 61
        Width = 121
        Height = 21
        TabOrder = 3
      end
      object EMass2: TEdit
        Left = 245
        Top = 61
        Width = 121
        Height = 21
        TabOrder = 4
      end
      object EAmount2: TEdit
        Left = 479
        Top = 61
        Width = 121
        Height = 21
        TabOrder = 5
      end
      object BStoichiometryCalculate: TButton
        Left = 3
        Top = 96
        Width = 75
        Height = 25
        Caption = 'Berechnen'
        TabOrder = 6
        OnClick = BStoichiometryCalculateClick
      end
    end
    object MolarMass: TTabSheet
      Caption = 'Molare Masse'
      ImageIndex = 3
      object LMolecularMassOutput: TLabel
        Left = 3
        Top = 40
        Width = 3
        Height = 13
      end
      object ESubstanceName: TEdit
        Left = 27
        Top = 21
        Width = 177
        Height = 21
        TabOrder = 0
        TextHint = 'Molek'#252'l eingeben'
      end
      object BMolecularMass: TButton
        Left = 210
        Top = 19
        Width = 75
        Height = 25
        Caption = 'Berechnen'
        TabOrder = 1
        OnClick = BMolecularMassClick
      end
    end
    object UnitConverter: TTabSheet
      Caption = 'Einheitenumrechnung'
      ImageIndex = 4
      object LAusgabeCalc: TLabel
        Left = 16
        Top = 102
        Width = 3
        Height = 13
      end
      object ECalc: TEdit
        Left = 11
        Top = 56
        Width = 137
        Height = 21
        TabOrder = 0
        TextHint = 'Wert eingeben'
        OnChange = ECalcChange
      end
      object CAusgangseinheit: TComboBox
        Left = 186
        Top = 56
        Width = 145
        Height = 21
        Style = csDropDownList
        TabOrder = 1
        OnChange = ECalcChange
      end
      object CEndEinheit: TComboBox
        Left = 186
        Top = 99
        Width = 145
        Height = 21
        Style = csDropDownList
        TabOrder = 2
        OnChange = ECalcChange
      end
      object BErgebnis: TButton
        Left = 16
        Top = 144
        Width = 107
        Height = 25
        Caption = 'Ergebnis kopieren'
        TabOrder = 3
        OnClick = BErgebnisClick
      end
      object CEinheitArt: TComboBox
        Left = 11
        Top = 19
        Width = 145
        Height = 21
        Style = csDropDownList
        TabOrder = 4
        OnChange = CEinheitArtChange
      end
    end
  end
end
