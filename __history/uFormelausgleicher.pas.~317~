﻿unit uFormelausgleicher;

interface

// authored by Emil Hausmann and Felix Neumann

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, StrUtils, Vcl.ComCtrls,
  Vcl.Grids, System.Json, Rest.Json, System.IoUtils, ShellAPI, Vcl.Buttons,
  Vcl.ExtCtrls, ClipBrd;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    FormulaBalancer: TTabSheet;
    EInput: TEdit;
    BStart: TButton;
    LOutput: TLabel;
    LOutputHead: TLabel;
    PeriodicTable: TTabSheet;
    StringGrid1: TStringGrid;
    Stoichiometry: TTabSheet;
    ESubstance1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    EMass1: TEdit;
    Label3: TLabel;
    EAmount1: TEdit;
    Label4: TLabel;
    ESubstance2: TEdit;
    Label5: TLabel;
    EMass2: TEdit;
    Label6: TLabel;
    EAmount2: TEdit;
    BStoichiometryCalculate: TButton;
    MolarMass: TTabSheet;
    ESubstanceName: TEdit;
    BMolecularMass: TButton;
    LMolecularMassOutput: TLabel;
    UnitConverter: TTabSheet;
    ECalc: TEdit;
    CAusgangseinheit: TComboBox;
    CEndEinheit: TComboBox;
    LAusgabeCalc: TLabel;
    BErgebnis: TButton;
    CEinheitArt: TComboBox;
    LHelp: TLabel;
    LWebsite: TLabel;
    procedure BStartClick(Sender: TObject);
    procedure EInputKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BStoichiometryCalculateClick(Sender: TObject);
    procedure BMolecularMassClick(Sender: TObject);
    procedure CEinheitArtChange(Sender: TObject);
    procedure ECalcChange(Sender: TObject);
    procedure LHelpClick(Sender: TObject);
    procedure LWebsiteClick(Sender: TObject);
    procedure BErgebnisClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

  // *******************************************************
  // *************** CUSTOM TYPE DEFINITIONS ***************
  // *******************************************************

  // Used for the charIsLetter function
type
  TCase = (Lowercase, Uppercase);

  // Used for the generateReaction and balanceFormula functions
type
  TError = (Success, Empty, NotPossible, Timeout);

  // Used for the seperateMolecule function to remember the current state
type
  TState = (MoleculeAmount, AtomAmount, AtomName);

  // Used for the TElement type
type
  TElementState = (Solid, Liquid, Gas, None3);

  // Used for the TEdits on the Stoichiometry TabSheet
type
  TField = (None, Substance1, Substance2, Mass1, Mass2, Amount1, Amount2);

  // Used for the Stoichiometry calculations
type
  TStoichiometryError = (None2, Success2, MultipleEmptyFields, CantFindAtom,
    SubstanceEditIsEmpty);

  // Essential type that is used for a lot of stuff
  // Used for example in the TMolecule type or the seperateMolecule function
type
  TAtom = class
  public
    amount: integer;
    name: string;
  end;

  // Essential type that is used for a lot of stuff
  // Used for example in the TReaction type or the seperateMolecule function
type
  TMolecule = class
  public
    amount: integer;
    atoms: TArray<TAtom>;
  end;

  // Used for the generateReaction function
type
  TReaction = class
  public
    reactants: TArray<TMolecule>;
    products: TArray<TMolecule>;
  end;

  // Type that stores data about each unit used to calculate the result of
  // the conversion
type
  TUnit = class
  public
    name: string;
    kategorie: string;
    default: Boolean;
    multiply: single;
    add: single;
  end;

  // Type that stores data about each element used to display in the table or
  // to calculate molecular masses
type
  TElement = class
  public
    ordnungszahl: integer;
    name: string;
    symbol: string;
    MolareMasse: single;
    nukleonenAnzahl: integer;
    dichte: single;
    schmelzpunkt: single;
    siedepunkt: single;
    elektronegativitaet: single;
    state: TElementState;
  end;

implementation

{$R *.dfm}
// ************************************************
// *************** PUBLIC VARIABLES ***************
// ************************************************

var
  error: TError;
  stoichiometryError: TStoichiometryError;
  elementsData: TArray<TElement>;
  einheitenData: TArray<TUnit>;
  einheitenCategoriesData: TArray<String>;

  // ************************************************
  // *************** HELPER FUNCTIONS ***************
  // ************************************************

  {
    Felix
    This function returns a string containing a subscript number of the
    original integer

    Params:
    amount: the number to be convertet to a subscript string

    Returns:
    string: the subscript string
  }
function getSubscriptString(amount: integer): string;
var
  amountstring: String;
  i: integer;

  // this constant array contains the subscript characters from 0 to 9
  // unfortunately, they are not displayed correctly in delphi
const
  subscript: array [0 .. 9] of string = ('₀', '₁', '₂', '₃', '₄', '₅', '₆', '₇',
    '₈', '₉');
begin
  amountstring := IntToStr(amount);
  Result := '';
  for i := 1 to Length(amountstring) do
  begin
    Result := Result + subscript[StrToInt(amountstring[i])];
  end;
end;

{
  Felix
  This function checks whether a char is a letter

  Params:
  casetocheck: the case to check for (Lowercase or Uppercase)
  char: the char to check

  Returns:
  boolean: whether the char is a letter
}
function charIsLetter(casetocheck: TCase; char: string): Boolean;
const
  lowercaseLetters: array [0 .. 25] of string = ('a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
    'v', 'w', 'x', 'y', 'z');
  uppercaseLetters: array [0 .. 25] of string = ('A', 'B', 'C', 'D', 'E', 'F',
    'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
    'V', 'W', 'X', 'Y', 'Z');
var
  i: integer;
begin
  for i := 0 to 25 do
  begin
    if casetocheck = Lowercase then
    begin
      if char = lowercaseLetters[i] then
      begin
        Result := True;
        exit;
      end;
    end;
    if casetocheck = Uppercase then
    begin
      if char = uppercaseLetters[i] then
      begin
        Result := True;
        exit;
      end;
    end;
  end;
end;

{
  Felix
  This function checks whether a string is an integer

  Params:
  value: the string to check

  Returns:
  boolean: whether the string is an integer
}
function isInteger(value: string): Boolean;
var
  unused: integer;
begin
  Result := TryStrToInt(value, unused);
end;

{
  Felix
  This function converts a string into a TMolecule which can later be used
  to balance formulas or get molecular masses

  Params:
  value: the string that shall be converted

  Returns:
  TMolecule: the seperated molecule
}
function seperateMolecule(value: string): TMolecule;
var
  atom: TAtom;
  molecule: TMolecule;
  state: TState;
  currentval: string;
  i: integer;
begin
  atom := TAtom.Create;
  molecule := TMolecule.Create;
  currentval := '';
  for i := 0 to Length(value) - 1 do
  begin
    if isInteger(value[i + 1]) then
    begin
      if i = 0 then
      begin
        state := MoleculeAmount;
        currentval := value[i + 1];
      end
      else
      begin
        if (state = MoleculeAmount) OR (state = AtomAmount) then
        begin
          currentval := currentval + value[i + 1];
        end
        else
        begin
          atom.name := currentval;
          currentval := value[i + 1];
          state := AtomAmount;
        end;
      end;
    end
    else
    begin
      if i = 0 then
      begin
        currentval := '1';
        state := MoleculeAmount;
      end;
      if charIsLetter(Uppercase, value[i + 1]) then
      begin
        if state = MoleculeAmount then
        begin
          molecule.amount := StrToInt(currentval);
        end
        else if state = AtomAmount then
        begin
          atom.amount := StrToInt(currentval);
          if atom.amount = 0 then
            atom.amount := 1;
          molecule.atoms := molecule.atoms + [atom];
          atom := TAtom.Create;
        end
        else if state = AtomName then
        begin
          atom.name := currentval;
          atom.amount := 1;
          molecule.atoms := molecule.atoms + [atom];
          atom := TAtom.Create;
        end;
        state := AtomName;
        currentval := value[i + 1];
      end
      else if charIsLetter(Lowercase, value[i + 1]) then
        currentval := currentval + value[i + 1];
    end;
  end;
  if state = AtomAmount then
  begin
    atom.amount := StrToInt(currentval);
    molecule.atoms := molecule.atoms + [atom];
  end
  else if state = AtomName then
  begin
    atom.name := currentval;
    if atom.amount = 0 then
      atom.amount := 1;
    molecule.atoms := molecule.atoms + [atom];
  end;
  Result := molecule;
end;

{
  Felix
  This function generates a TReaction from a input string

  Params:
  value: the string to convert the reaction from

  Returns:
  TReaction: the reaction that can be used for further calculations, for
  example balancing formulas
}
function generateReaction(value: string): TReaction;
var
  reactants, products: TArray<string>;
  reaction: TReaction;
  i: integer;
begin
  if value = '' then
  begin
    error := Empty;
    exit;
  end;
  reactants := SplitString(SplitString(value, '=')[0], '+');
  products := SplitString(SplitString(value, '=')[1], '+');
  reaction := TReaction.Create;
  for i := 0 to Length(reactants) - 1 do
  begin
    reaction.reactants := reaction.reactants + [seperateMolecule(reactants[i])];
  end;
  for i := 0 to Length(products) - 1 do
  begin
    reaction.products := reaction.products + [seperateMolecule(products[i])];
  end;
  Result := reaction;
end;

{
  Felix
  This function sorts a TArray containing TAtom objects

  Params:
  value: the TAtom array

  Returns:
  TArray<TAtom>: the sorted TAtom array
}
function sortAtomArray(value: TArray<TAtom>): TArray<TAtom>;
var
  swapped: Boolean;
  i: integer;
  m: TAtom;
begin
  repeat
  begin
    swapped := False;
    for i := 1 to Length(value) - 1 do
    begin
      if value[i - 1].name > value[i].name then
      begin
        m := value[i];
        value[i] := value[i - 1];
        value[i - 1] := m;
      end;
    end;
  end;
  until (not swapped);
  Result := value;
end;

{
  Felix
  This function checks whether a reaction is balanced

  Params:
  reactantsElements: array containing TAtom objects from the reactants side
  productsElements: array containing TAtom objects from the products side

  Returns:
  boolean: whether the reaction is balanced
}
function isBalanced(reactantsElements: TArray<TAtom>;
  productsElements: TArray<TAtom>): Boolean;
var
  i: integer;
begin
  Result := True;
  for i := 0 to Length(reactantsElements) - 1 do
    if reactantsElements[i].amount <> productsElements[i].amount then
      Result := False;
end;

{
  Felix
  This function generates a TAtom array from a TMolecule array by counting
  the amount of the atoms

  Params:
  value: array containing TMolecule objects

  Returns:
  TArray<TAtom>: array containing TAtom objects
}
function getElementsFromReactionHalf(value: TArray<TMolecule>): TArray<TAtom>;
var
  i, j, k: integer;
  added: Boolean;
  atom: TAtom;
begin
  for i := 0 to Length(value) - 1 do
  begin
    for j := 0 to Length(value[i].atoms) - 1 do
    begin
      added := False;
      for k := 0 to Length(Result) - 1 do
      begin
        if Result[k].name = value[i].atoms[j].name then
        begin
          Result[k].amount := Result[k].amount + value[i].amount *
            value[i].atoms[j].amount;
          added := True;
        end;
      end;
      if not added then
      begin
        atom := TAtom.Create;
        atom.name := value[i].atoms[j].name;
        atom.amount := value[i].amount * value[i].atoms[j].amount;
        Result := Result + [atom];
      end;
    end;
  end;
end;

{
  Felix
  This function balances reactions
  It is the centerpiece of this program and already very complex
  The version in the web app is even more advanced, even taking priorities of
  molecules into consideration. This is one of the limits of the delphi code.
  More simple reactions should still be balanced perfectly.

  Params:
  value: the string containing the reaction

  Returns:
  TReaction: the balanced reaction
}
function balanceFormula(value: string): TReaction;
var
  reaction: TReaction;
  reactantsElements, productsElements: TArray<TAtom>;
  equal: Boolean;
  i, counter: integer;
  j: integer;
  m: integer;
  n: integer;
  s: integer;
begin
  reaction := generateReaction(value);
  reactantsElements := getElementsFromReactionHalf(reaction.reactants);
  productsElements := getElementsFromReactionHalf(reaction.products);
  if Length(reactantsElements) <> Length(productsElements) then
  begin
    error := NotPossible;
    exit;
  end;
  reactantsElements := sortAtomArray(reactantsElements);
  productsElements := sortAtomArray(productsElements);
  equal := True;
  for i := 0 to Length(reactantsElements) - 1 do
  begin
    if equal then
      equal := reactantsElements[i].name = productsElements[i].name;
  end;
  if not equal then
  begin
    error := NotPossible;
    exit;
  end;
  counter := 0;
  while not isBalanced(reactantsElements, productsElements) do
  begin
    Inc(counter);
    if counter > 24 then
    begin
      error := Timeout;
      exit;
    end;
    for i := 0 to Length(reactantsElements) - 1 do
    begin
      if reactantsElements[i].amount < productsElements[i].amount then
      begin
        for j := 0 to Length(reaction.reactants) - 1 do
        begin
          for m := 0 to Length(reaction.reactants[j].atoms) - 1 do
          begin
            if reaction.reactants[j].atoms[m].name = reactantsElements[i].name
            then
            begin
              Inc(reaction.reactants[j].amount);
              for n := 0 to Length(reaction.reactants[j].atoms) - 1 do
              begin
                for s := 0 to Length(reactantsElements) - 1 do
                begin
                  if reactantsElements[s].name = reaction.reactants[j].atoms[n].name
                  then
                    reactantsElements[s].amount := reactantsElements[s].amount +
                      reaction.reactants[j].atoms[n].amount;
                end;
              end;
            end;
          end;
        end;
      end
      else if reactantsElements[i].amount > productsElements[i].amount then
      begin
        for j := 0 to Length(reaction.products) - 1 do
        begin
          for m := 0 to Length(reaction.products[j].atoms) - 1 do
          begin
            if reaction.products[j].atoms[m].name = productsElements[i].name
            then
            begin
              Inc(reaction.products[j].amount);
              for n := 0 to Length(reaction.products[j].atoms) - 1 do
              begin
                for s := 0 to Length(productsElements) - 1 do
                begin
                  if productsElements[s].name = reaction.products[j].atoms[n].name
                  then
                    productsElements[s].amount := productsElements[s].amount +
                      reaction.products[j].atoms[n].amount;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  error := Success;
  Result := reaction;
end;

{
  Felix
  This function uses an input string, sends it to the balanceFormula function
  and then generates the string to output to the user

  Params:
  value: input string

  Returns:
  string: the output string for the balanced formula
}
function getBalancedFormulaString(value: string): string;
var
  balancedFormula: TReaction;
  i: integer;
  j: integer;
begin
  Result := '';
  if value = '' then
  begin
    error := Empty;
    exit;
  end;
  balancedFormula := balanceFormula(value);
  if error <> Success then
  begin
    exit;
  end;
  for i := 0 to Length(balancedFormula.reactants) - 1 do
  begin
    if balancedFormula.reactants[i].amount <> 1 then
      Result := Result + IntToStr(balancedFormula.reactants[i].amount);
    for j := 0 to Length(balancedFormula.reactants[i].atoms) - 1 do
    begin
      Result := Result + balancedFormula.reactants[i].atoms[j].name;
      if balancedFormula.reactants[i].atoms[j].amount <> 1 then
        Result := Result + getSubscriptString(balancedFormula.reactants[i].atoms
          [j].amount);
    end;
    if i <> Length(balancedFormula.reactants) - 1 then
      Result := Result + ' + ';
  end;
  Result := Result + ' → ';
  for i := 0 to Length(balancedFormula.products) - 1 do
  begin
    if balancedFormula.products[i].amount <> 1 then
      Result := Result + IntToStr(balancedFormula.products[i].amount);
    for j := 0 to Length(balancedFormula.products[i].atoms) - 1 do
    begin
      Result := Result + balancedFormula.products[i].atoms[j].name;
      if balancedFormula.products[i].atoms[j].amount <> 1 then
        Result := Result + getSubscriptString(balancedFormula.products[i].atoms
          [j].amount);
    end;
    if i <> Length(balancedFormula.products) - 1 then
      Result := Result + ' + ';
  end;
end;

{
  Emil
  This procedure gets called when the CEinheitArt ComboBox gets changed
  It puts the new possible unit names into the other two ComboBoxes

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.CEinheitArtChange(Sender: TObject);
var
  i: integer;
begin
  CAusgangseinheit.Items.Clear;
  CEndEinheit.Items.Clear;
  for i := 0 to Length(einheitenData) - 1 do
  begin
    if einheitenData[i].kategorie = CEinheitArt.Text then
    begin
      CAusgangseinheit.Items.add(einheitenData[i].name);
      CEndEinheit.Items.add(einheitenData[i].name);
    end;
  end;
end;

{
  Felix
  This procedure gets called when the BStart Button gets clicked
  It calles the getBalancedFormulaString and outputs errors

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.BStartClick(Sender: TObject);
begin
  LOutputHead.Visible := False;
  LOutput.Caption := getBalancedFormulaString(EInput.Text);
  case error of
    Empty:
      LOutput.Caption := 'Bitte geben Sie etwas ein!';
    NotPossible:
      LOutput.Caption :=
        'Auf beiden Seiten der Reaktion müssen die selben Atome vorkommen.';
    Timeout:
      LOutput.Caption := 'Ausgleichen der Reaktion nicht möglich.';
    Success:
      LOutputHead.Visible := True;
  end;

end;

{
  Felix
  This function converts the input string into a TMolecule and gets the
  molecular mass of it.

  Params:
  substance: input string

  Returns:
  single: the molecular mass of the substance
}
function getMolecularMass(substance: string): single;
var
  molecule: TMolecule;
  i: integer;
  j: integer;
begin
  Result := 0;
  molecule := seperateMolecule(substance);
  for i := 0 to Length(molecule.atoms) - 1 do
  begin
    for j := 0 to Length(elementsData) - 1 do
      if elementsData[j].symbol = molecule.atoms[i].name then
      begin
        Result := Result + molecule.atoms[i].amount * elementsData[j]
          .MolareMasse;
        break;
      end;
    if j = Length(elementsData) then
    begin
      stoichiometryError := CantFindAtom;
      exit;
    end;
  end;
end;

procedure TForm1.BErgebnisClick(Sender: TObject);
begin
  Clipboard.AsText := LAusgabeCalc.Caption;
end;

{
  Felix
  This procedure gets called when the BMolecularMass Button gets clicked
  It calles the getMolecularMass and outputs the result

  Params:
  Sender: the TObject calling the procedure
}

procedure TForm1.BMolecularMassClick(Sender: TObject);
begin
  LMolecularMassOutput.Caption :=
    FloatToStrF(getMolecularMass(ESubstanceName.Text), ffFixed, 8, 2) +
    ' g/mol';
end;

{
  Felix
  This function uses multiple input fields to calculate mass 1

  Params:
  substance1: the first substance (string)
  substance2: the second substance (string)
  mass2: the second mass (single)
  amount1: the first amount (integer)
  amount2: the first amount (integer)

  Returns:
  single: the output mass 1
}
function calculateMass1(Substance1: string; Substance2: string; Mass2: single;
  Amount1: integer; Amount2: integer): single;
begin
  Result := (getMolecularMass(Substance1) * Amount1 * Mass2) /
    (getMolecularMass(Substance2) * Amount2);
end;

{
  Felix
  This function uses multiple input fields to calculate mass 2

  Params:
  substance1: the first substance (string)
  substance2: the second substance (string)
  mass1: the first mass (single)
  amount1: the first amount (integer)
  amount2: the first amount (integer)

  Returns:
  single: the output mass 2
}
function calculateMass2(Substance1: string; Substance2: string; Mass1: single;
  Amount1: integer; Amount2: integer): single;
begin
  Result := (getMolecularMass(Substance2) * Amount2 * Mass1) /
    (getMolecularMass(Substance1) * Amount1);
end;

{
  Felix
  This function uses multiple input fields to calculate amount 1

  Params:
  substance1: the first substance (string)
  substance2: the second substance (string)
  mass1: the first mass (single)
  mass2: the second mass (single)
  amount2: the second amount (integer)

  Returns:
  single: the output amount 1
}
function calculateAmount1(Substance1: string; Substance2: string; Mass1: single;
  Mass2: single; Amount2: integer): single;
begin
  Result := getMolecularMass(Substance2) * Mass1 * Amount2 /
    (getMolecularMass(Substance1) * Mass2);
end;

{
  Felix
  This function uses multiple input fields to calculate amount 2

  Params:
  substance1: the first substance (string)
  substance2: the second substance (string)
  mass1: the first mass (single)
  mass2: the second mass (single)
  amount1: the first amount (integer)

  Returns:
  single: the output amount 2
}
function calculateAmount2(Substance1: string; Substance2: string; Mass1: single;
  Mass2: single; Amount1: integer): single;
begin
  Result := getMolecularMass(Substance1) * Mass2 * Amount1 /
    (getMolecularMass(Substance2) * Mass1);
end;

{
  Felix
  This procedure gets called when the BStoichiometryCalculate Button gets
  clicked
  It coordinates the calculation of the stoichiometry fields

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.BStoichiometryCalculateClick(Sender: TObject);
var
  emptyField: TField;
begin
  emptyField := None;
  stoichiometryError := None2;
  if ESubstance1.Text = '' then
    emptyField := Substance1;
  if ESubstance2.Text = '' then
    if emptyField = None then
      emptyField := Substance2
    else
      stoichiometryError := MultipleEmptyFields;
  if EMass1.Text = '' then
    if emptyField = None then
      emptyField := Mass1
    else
      stoichiometryError := MultipleEmptyFields;
  if EMass2.Text = '' then
    if emptyField = None then
      emptyField := Mass2
    else
      stoichiometryError := MultipleEmptyFields;
  if EAmount1.Text = '' then
    if emptyField = None then
      emptyField := Amount1
    else
      stoichiometryError := MultipleEmptyFields;
  if EAmount2.Text = '' then
    if emptyField = None then
      emptyField := Amount2
    else
      stoichiometryError := MultipleEmptyFields;
  if (stoichiometryError = MultipleEmptyFields) OR (emptyField = None) then
  begin
    ShowMessage('Es muss genau ein leeres Feld geben.');
    exit;
  end;
  case emptyField of
    Mass1:
      EMass1.Text := FloatToStrF(calculateMass1(ESubstance1.Text,
        ESubstance2.Text, StrToFloat(EMass2.Text), StrToInt(EAmount1.Text),
        StrToInt(EAmount2.Text)), ffFixed, 8, 2);
    Mass2:
      EMass2.Text := FloatToStrF(calculateMass2(ESubstance1.Text,
        ESubstance2.Text, StrToFloat(EMass1.Text), StrToInt(EAmount1.Text),
        StrToInt(EAmount2.Text)), ffFixed, 8, 2);
    Amount1:
      EAmount1.Text := FloatToStrF(calculateAmount1(ESubstance1.Text,
        ESubstance2.Text, StrToFloat(EMass1.Text), StrToFloat(EMass2.Text),
        StrToInt(EAmount2.Text)), ffFixed, 8, 0);
    Amount2:
      EAmount2.Text := FloatToStrF(calculateAmount2(ESubstance1.Text,
        ESubstance2.Text, StrToFloat(EMass1.Text), StrToFloat(EMass2.Text),
        StrToInt(EAmount1.Text)), ffFixed, 8, 0);
  else
    begin
      stoichiometryError := SubstanceEditIsEmpty;
    end;
  end;
  case stoichiometryError of
    MultipleEmptyFields:
      ShowMessage('Es darf nur ein leeres Feld geben.');
    CantFindAtom:
      ShowMessage('Eines der Atome kann nicht gefunden werden.');
    SubstanceEditIsEmpty:
      ShowMessage('Beide Stoff-Eingabefelder müssen ausgefüllt sein.');
  end;
end;

{
  Emil
  This function gets a unit by the name of the unit

  Params:
  name: the input string

  Returns:
  TUnit: the unit
}
function getUnitByName(name: string): TUnit;
var
  _unit: TUnit;
begin
  Result := nil;
  for _unit in einheitenData do
  begin
    if _unit.name = name then
    begin
      Result := _unit;
      exit;
    end;
  end;
  if Result = nil then
    ShowMessage('An unexpected error occurred!');
end;

{
  Emil
  This function gets the default unit by a category

  Params:
  category: the category input string

  Returns:
  TUnit: the default unit
}
function getDefaultUnitForCategory(category: string): TUnit;
var
  _unit: TUnit;
begin
  Result := nil;
  for _unit in einheitenData do
  begin
    if (_unit.kategorie = category) AND _unit.default then
    begin
      Result := _unit;
      exit;
    end;
  end;
  if Result = nil then
    ShowMessage('An unexpected error occurred!');
end;

{
  Emil
  This procedure converts the start unit to the target unit
}
procedure calculate();
var
  x: single;
var
  unit1, unit2, defaultUnit: TUnit;
begin
  // check that edit is not empty
  if Form1.ECalc.Text = '' then
  begin
    Form1.LAusgabeCalc.Caption := '';
    exit;
  end;

  // check that cbs arent empty
  if (Form1.CAusgangseinheit.Text = '') OR (Form1.CEndEinheit.Text = '') then
  begin
    exit;
  end;

  // output the input if units are the same
  if Form1.CAusgangseinheit.Text = Form1.CEndEinheit.Text then
  begin
    Form1.LAusgabeCalc.Caption := Form1.ECalc.Text;
    exit;
  end;

  // get in- and output units
  unit1 := getUnitByName(Form1.CAusgangseinheit.Text);
  unit2 := getUnitByName(Form1.CEndEinheit.Text);

  // get default unit
  defaultUnit := getDefaultUnitForCategory
    (getUnitByName(Form1.CAusgangseinheit.Text).kategorie);

  // read in start value
  x := StrToFloat(Form1.ECalc.Text);

  // convert back to default unit
  if not unit1.default then
  begin
    x := x - unit1.add;
    x := x / unit1.multiply;
  end;

  // convert from default unit to result unit
  if not unit2.default then
  begin
    x := x * unit2.multiply;
    x := x + unit2.add;
  end;

  // output result
  Form1.LAusgabeCalc.Caption := FloatToStrF(x, ffFixed, 10, 6);
end;

{
  Emil
  This procedure gets called when the ECalc Edit gets changed
  It calls the calculate procedure

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.ECalcChange(Sender: TObject);
begin
  calculate;
end;

{
  Felix
  This procedure gets called when a key gets hit by the user

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.EInputKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // 13 = enter key
  if Key = 13 then
    BStartClick(nil);
end;

{
  Felix
  This function checks whether a string array contains a string

  Params:
  arr: the string array
  query: the string that the function searches for

  Returns:
  boolean: whether the array contains the string
}
function arrayContains(arr: TArray<String>; query: string): Boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to Length(arr) - 1 do
    if arr[i] = query then
    begin
      Result := True;
      exit;
    end;
end;

{
  Felix
  This procedure gets called when the Form gets created
  It reads in the data files, puts data into the string grid and does other
  initial setup

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.FormCreate(Sender: TObject);
const
  JSONFilePath = 'data.json';
  JSONEinheiten = 'units.json';

var
  fileContent: string;
  values: TJSONArray;
  value: TJSONValue;
  element: TElement;
  einheit: TUnit;
  i: integer;

begin
  fileContent := TFile.ReadAllText(JSONFilePath);
  values := TJSONObject.ParseJSONValue(fileContent) as TJSONArray;
  for value in values do
  begin
    element := TElement.Create;
    element.ordnungszahl := value.GetValue<integer>('ordnungszahl');
    element.name := value.GetValue<string>('name');
    element.symbol := value.GetValue<string>('symbol');
    element.MolareMasse := value.GetValue<single>('molareMasse');
    element.nukleonenAnzahl := value.GetValue<integer>('nukleonenAnzahl');
    element.dichte := value.GetValue<single>('dichte');
    if value.FindValue('schmelzpunkt') <> nil then
      element.schmelzpunkt := value.GetValue<single>('schmelzpunkt');
    if value.FindValue('siedepunkt') <> nil then
      element.siedepunkt := value.GetValue<single>('siedepunkt');
    if value.FindValue('elektronegativitaet') <> nil then
      element.elektronegativitaet := value.GetValue<single>
        ('elektronegativitaet');
    element.state := None3;
    if value.FindValue('state') <> nil then
    begin
      if value.GetValue<string>('state') = 'solid' then
        element.state := Solid;
      if value.GetValue<string>('state') = 'liquid' then
        element.state := Liquid;
      if value.GetValue<string>('state') = 'gas' then
        element.state := Gas;
    end;
    if element.state = None3 then
    begin
      if element.siedepunkt < 20 then
      begin
        element.state := Gas;
      end
      else if element.schmelzpunkt < 20 then
      begin
        element.state := Liquid;
      end
      else
        element.state := Solid;
    end;
    elementsData := elementsData + [element];
  end;
  StringGrid1.Cells[0, 0] := 'Ordnungszahl';
  StringGrid1.Cells[1, 0] := 'Symbol';
  StringGrid1.Cells[2, 0] := 'Name';
  StringGrid1.Cells[3, 0] := 'Molare Masse (g/mol)';
  StringGrid1.Cells[4, 0] := 'Anzahl der Nukleonen';
  StringGrid1.Cells[5, 0] := 'Dichte';
  StringGrid1.Cells[6, 0] := 'Schmelzpunkt (°C)';
  StringGrid1.Cells[7, 0] := 'Siedepunkt (°C)';
  StringGrid1.Cells[8, 0] := 'Elektronegativität';
  StringGrid1.Cells[9, 0] := 'Zustand (20°C)';
  for i := 0 to Length(elementsData) - 1 do
  begin
    StringGrid1.Cells[0, i + 1] := IntToStr(elementsData[i].ordnungszahl);
    StringGrid1.Cells[1, i + 1] := elementsData[i].symbol;
    StringGrid1.Cells[2, i + 1] := elementsData[i].name;
    StringGrid1.Cells[3, i + 1] := FloatToStrF(elementsData[i].MolareMasse,
      ffFixed, 8, 2);
    StringGrid1.Cells[4, i + 1] := IntToStr(elementsData[i].nukleonenAnzahl);
    StringGrid1.Cells[5, i + 1] := FloatToStrF(elementsData[i].dichte,
      ffFixed, 8, 2);
    if elementsData[i].schmelzpunkt = 0 then
    begin
      StringGrid1.Cells[6, i + 1] := '-'
    end
    else
      StringGrid1.Cells[6, i + 1] := FloatToStrF(elementsData[i].schmelzpunkt,
        ffFixed, 8, 2);
    if elementsData[i].siedepunkt = 0 then
    begin
      StringGrid1.Cells[7, i + 1] := '-'
    end
    else
      StringGrid1.Cells[7, i + 1] := FloatToStrF(elementsData[i].siedepunkt,
        ffFixed, 8, 2);
    if elementsData[i].elektronegativitaet = 0 then
    begin
      StringGrid1.Cells[8, i + 1] := '-';
    end
    else
    begin
      StringGrid1.Cells[8, i + 1] :=
        FloatToStrF(elementsData[i].elektronegativitaet, ffFixed, 8, 1);
    end;
    case elementsData[i].state of
      Solid:
        StringGrid1.Cells[9, i + 1] := 'fest';
      Liquid:
        StringGrid1.Cells[9, i + 1] := 'flüssig';
      Gas:
        StringGrid1.Cells[9, i + 1] := 'gasförmig';
    end;
  end;
  fileContent := TFile.ReadAllText(JSONEinheiten);
  values := TJSONObject.ParseJSONValue(fileContent) as TJSONArray;
  for value in values do
  begin
    einheit := TUnit.Create;
    einheit.name := value.GetValue<string>('Name');
    einheit.kategorie := value.GetValue<string>('Kategorie');
    if not arrayContains(einheitenCategoriesData, einheit.kategorie) then
      einheitenCategoriesData := einheitenCategoriesData + [einheit.kategorie];
    einheit.default := value.GetValue<Boolean>('Standard');
    if not einheit.default then
    begin
      einheit.multiply := value.GetValue<single>('Multiply');
      einheit.add := value.GetValue<single>('Add');
    end;
    einheitenData := einheitenData + [einheit];

  end;
  for i := 0 to Length(einheitenCategoriesData) - 1 do
  begin
    CEinheitArt.Items.add(einheitenCategoriesData[i]);
  end;
  CEinheitArt.ItemIndex := 0;
  for i := 0 to Length(einheitenData) - 1 do
  begin
    if einheitenData[i].kategorie = einheitenCategoriesData
      [CEinheitArt.ItemIndex] then
    begin
      CAusgangseinheit.Items.add(einheitenData[i].name);
      CEndEinheit.Items.add(einheitenData[i].name);
    end;
  end;
end;

{
  This procedure gets called when the HelpLink gets clicked
  It opens the help page

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.LHelpClick(Sender: TObject);
begin
  ShellExecute(0, 'open', PChar('https://chemistryutil.web.app/help'), nil, nil,
    SW_SHOWNORMAL);
end;

{
  This procedure gets called when the WebsiteLink gets clicked
  It opens the main page

  Params:
  Sender: the TObject calling the procedure
}
procedure TForm1.LWebsiteClick(Sender: TObject);
begin
  ShellExecute(0, 'open', PChar('https://chemistryutil.web.app/'), nil, nil,
    SW_SHOWNORMAL);
end;

end.
